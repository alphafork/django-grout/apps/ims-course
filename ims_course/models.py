from django.db import models
from ims_base.models import AbstractBaseDetail
from reusable_models import get_model_from_string


class Course(AbstractBaseDetail):
    DURATION_UNITS = [("h", "hour"), ("d", "day"), ("w", "week"), ("m", "month")]

    course_code = models.CharField(max_length=255, null=True, blank=True)
    fee = models.PositiveIntegerField(null=True, blank=True)
    is_registration_open = models.BooleanField(default=True)
    duration_days = models.PositiveSmallIntegerField(null=True, blank=True)
    introduced_date = models.DateField(null=True, blank=True)
    subjects = models.ManyToManyField(get_model_from_string("SUBJECT"), blank=True)
