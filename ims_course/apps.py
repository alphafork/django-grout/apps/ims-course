from django.apps import AppConfig


class IMSCourseConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_course"

    model_strings = {
        "COURSE": "Course",
    }
